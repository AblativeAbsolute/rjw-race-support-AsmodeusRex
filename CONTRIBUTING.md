# VERY WIP

# Issues
https://gitgud.io/AsmodeusRex/rjw-race-support/-/issues
## Create a new issue
The most efficient (and probably fastest) way to get support added for a race you want is to open an issue. Clearly state what mod you want support for, make sure it isn't in the list of supported mods, and put a link to the steam workshop page if it has one.

You should also open an issue if there is a problem with existing race support or the mod in general.
## Solve an issue
Help is of course always appreciated. Make sure you follow the guidelines in the rest of this file.

# Add support for a new mod
There are two methods to add support for a new mod. If the mod isn't doing anything super wild, the first method is preferred. Only use the second method if you can't use the first method.

No matter what method you use, you will need to know the `defName` of the races you are adding support for. This can be found in the XML files of the mod that adds those races. The path to the files you are looking for will be something like: `ModDirectory/1.4/Defs/ThingDefs_Races/animals.xml`.

## Setting up
### Fork the repository
Create a GitGud/Sapphire account. Hit the fork button.
### Installing tools
Install [Visual Studio Code](https://code.visualstudio.com/). This is to make use of the spelling dictionary and other VSCode specific configuration.

Install the VSCode extensions "Code Spell Checker" and "rust-analyzer".

Install [Rust](https://www.rust-lang.org/learn/get-started). Don't worry about learning rust, all you'll be doing in it is data definition from pre-defined templates.

## Method 1: Rust
This method involves adding new entries to the list of parts and races in the Rust source files (`.rs`).
### Racegroups
A racegroup is a group of... races that all share the same characteristics. For example, both a Cassowary and an Ostrich are in the avian animal group.
At the bottom of the file `generator/src/racegroup.rs` (around line 100 or so) you will find the lists of racegroups. Find the racegroup that has the right parts and skin type for each race you want to support. Simply add the `defName` of each race to the appropriate racegroup. Make sure to properly put the `defName` in alphabetical order. The scheme is as follows:
* Vanilla animals (e.g. `Cobra`)
* Mod animals with a mod prefix (e.g. `AA_BloodShrimp`)
* Mod animals without a mod prefix (e.g. `Rakk`)

Remember that each category is in alphabetical order.

There! You're already done. That's all that you need to do for most patches. The generator will do all the rest for you. Go ahead and skip to the PRs section.

If you couldn't find an appropriate racegroup for your race, look again. If you really can't find one, the process is much more complicated.

#### **Adding a new racegroup**
*Actual instructions coming soon*

Copy and paste the right racegroup def/function call into the right place alphabetically. Use `RaceGroup::standard` for humans and `RaceGroup::standard_animal` for animals. Edit the fields.
#### **Tags**
These are the available tags/skin types for a race group.
* Chitin
* Demon
* Feathers
* Fur
* Plant
* Robot
* Scales
* Skin
* Slime

### Parts
Adding to the Rust source files is the only way to create new parts. Technically you can make the xmls by hand, but you have to remember to add a single very rote and repetitive entry to at least 3 different files, and it quickly becomes unmaintainable. If you can't figure out how to use the generator to make what you want, don't hesitate to open an issue.

First, figure out if the part you are adding is for a human or animal, or if it is breasts for either. Make sure you put your part in the correct list, or it won't work.

Next, open generator/src/parts.rs. Find the correct list (starting around line 200), copy and edit the template, making sure to put it in alphabetical order by species name (animal parts are further grouped into mammal and non-mammal).

**Template**

	Part::standard("a", PartType::Penis, "species",
		"Part Description",
		vec!["Tag", "AnotherTag"]
	).unwrap(),

#### **Properties**

Penis:
* Girthy/Thin
* Long/Small
* Artificial
* Flared
* Humanlike (automatically added to the human group, generally don't use)
* Internal (retractable)
* Knotted
* Multiple (has multiple members, such as a tentacle or hemi-penis)
* Prehensile
* Rigid (has a baculum)
* Sheathed
* Solid (is made of a solid inorganic material, such as stone or metal. Not to be confused with rigid)
* Tapered

Hole:
* Loose/Tight
* Deep

Either:
* Barbed
* Glowing
* Resizable (grows or shrinks to fit)
* Ridged (not to be confused with rigid)
* STDImmune (immune to STDs)

# PRs
After you make changes, you need to make a PR. This should get you started: https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/getting-started/about-collaborative-development-models.