Write-Output "Wait! Did you update LATEST_CHANGELOG?"
pause

$l = ".\LATEST_CHANGELOG"
$c = ".\changelog.txt"
$m = "Update to " + $args[0]

Write-Output ("Deploying version " + $args[0])

$a = Get-Content $l
$b = $a | ForEach-Object { "* " + $_ }
$b | Set-Content $l

Write-Output $b

$(
	($args[0])
	($a | ForEach-Object { "-" + $_ })
	("")
	(Get-Content $c -Raw)
) | Set-Content $c

git add $l $c
git commit -q -m $m
git tag $args[0]
git push origin $args[0]