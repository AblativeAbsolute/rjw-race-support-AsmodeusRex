11.4.1
-Moved ATK Gnollmorph to feline group
-Added build script

11.4.0
-Added deploy script
-Fixed LoadFolders for Revia and Moyo
-Added changelogs to release page

11.3.3
-Added support for DLC races

11.3.2
-Hotfixed Gods of Elona, Ooka miko, KMH

11.3.1
-Fixed Emperor of dragon, Gods of elona, Ooka miko, and kemomimihouse
-Added support for insect girls

11.3.0
-Fixed some viviparous species laying chicken eggs
-Added PokeWorld support
-Improved VAE Caves support

11.2.7
-Fixed Androids race support

11.2.6
-Improved Yokai and AACP support
-Fixed Giant Race
-Removed outdated Nam' Hellhound and Epona support

11.2.5
-Fixed horses not having equine anus
-Fixed race support for Adeptus Xenobiologis: Orkoids
-Removed support for dead Simple Ogre and Momu races

11.2.4
-Fixed an error from HAR apparel support
-Added genderless slime animal group
-Tweaked COC Byakhee race

11.2.3
-Added defs for missing vanilla races

11.2.2
-Beast Man Tribes improvements
-Removed colonist milking from insect and reptile people
-Removed trait stuff as it does not fit the mod's scope

11.2.1
-Removed janky support for AvP
-Removed support for Cribby's Avians
-Fixed some issues with Alpha Animals
-Fixed ACP Red Panda and Megascorpion
-Witcher Monster Hunt improvements
-Archotech group improvements
-Insect person improvements
-Thrumbo group improvements

11.2.0
-1.4 update
-Changed old Forsakens Fauna defs to Alpha Animals
-Fixed some incorrectly ordered defs

11.1.0
-Changed lots of rote and error prone xml defs into a Rust script that generates the files
-Fixed lots of typos
-Removed lots of uses of the "Generic" parts
-Removed dead and unused xmls
-Shuffled around a ton of xmls to be more intuitive and similar to the structure rjw uses

11.0.0
-Takeover by AsmodeusRex.
-Cleaned up lots of formatting and spelling issues.
-Updated support for many mods.
-Reworked most species to be more correct or lore-friendly.
-Removed redundant or outdated support modules
-Removed the single existing hybrid support module pending a more complete solution
-Added support for VE Genetics
-Restored rat gestation time
-Feles - Felines of the Rim support

10.2.0
-Race to the Rim Egg support (Stormy Rain)
-Removed all disabled content

10.1.0
-Merged redraster commits

10.0.0
-Removed several useful patches that do not patch races to Ab Patches.
-Removed Breeders Delight to Ab Patches.
-Removed some patches entirely.
-Cleanup.

9.9.0
-Warhammer Dryad support (ShauaPuta)
-SoS2 Support update (Mobile Kek Suit)
-Disabled several patches {enough fixing other peoples mods}
-Changed Vanilla Animal Support into a Patch
-Clean up

9.8.0
-Arthropods update (ShauaPuta)
-Skrix Race support (ShauaPuta)
-Korean translation upade (jhagatai)

9.7.0
-Mark Steam Milkable colonists incompatible.
-Kobold Milk Patch (Caufendra Sunclaws)
-Argonian Milk Patch (Caufendra Sunclaws)
-Protogen Milk Patch (Caufendra Sunclaws)
-PawnBold Milk Patch (Caufendra Sunclaws)

9.6.0
-Kurin Milk Patch
-Rabbie Milk Patch
-Bun Race Milk Patch (Hawkeye32)
-Ferian Mil Patch (Hawkeye32)

9.5.0
-SoS2 Hologram Race Support (not for Proper Spawn method but rather Dev mode type spawning.)
-Updating and Clean up of Files.
-RatKin Milk patch.

9.4.1
-conditional loading for some folders (Avali and Avians)

9.4.0
-ShadowRim Race Support
-Merged Ed86's changes adding Egg support for Avians and Avali.

9.3.1
-Clean up.

9.3.0
-Xenohumans - Anthromorphs Milk Support.

9.2.2
-update Fogotten realms support
-Marked PC as incompatible

9.2.1
-Update Kemomimihouse Support.

9.2.0
-Infertile Immortals show 0% Fertility

9.1.2
-More Milk Patch sillyness.

9.1.1
-Update Llisceans PawnKind

9.1.0
-Remade the Milk patch. Maybe it works now ?

9.0.4
-Fixed typo in Elder Things Race Support (only affected Unified XML Export)

9.0.3
-Fix Mac Paths

9.0.2
-Fixed Folder strucuter of one Patch set

9.0.1
-Fixed Typo caused by Git bug

9.0.0
-Korean (by: jhagatai)
-Updated Revia Patch
-Updated Milk patch to work better ?
-Fixed Ovipore Private Parts ?
-removed Textures
-Changed folder structure
-Relocated the (3x2) Table

8.10.4
-Better catch all Milk Patch ?
-Changed Revia to no longer spawn Geared Nymphs

8.10.3
-Fix Trait Value patch?

8.10.2
-More cleanup
-Increased Kijin Sex Drive

8.10.1
-Fixed ID detection for SYR Traits

8.10.0
-()()Cribby's Avians()() Race Support
-SYR Traits Mod support
-Editied some patches to be cleaner

8.9.1
-Template clarity update
-Folder structure change

8.9.0
-update ReadMe for contribution
-Added Template file in Contributor content

*Nejarrie
-updated AlphaAnimals Race Support
-added more animals to EarLike Animals
-Palamute Race Support
-Nexomon Race Support

8.8.1
-Clean up and downsizing
-Mincho support update

8.8.0
-Added Angels and Devils Race Support
-Added Rape AI to RoM Vampire Shadow Tentacle monster (maybe it works ?)
-cleaned up code of Race Supports

*ShauaPuta
-Updated WaSkeleton Race Support
*Nejarrie
-Genetic Rim Race Support

8.7.0
-Change Arachne to Birth Young and not use eggs
-cleaned up Androids race Support file
-cleaned up Kolra Oni race support file

 *𝕶𝖚𝖗𝖔 𝖓𝖔 𝕶𝖊𝖎𝖞𝖆𝖐𝖚𝖘𝖍𝖆 (added support for)
-Beetle People
-Epona The centaur Race
-Fantasy Goblin
-The Giant Race
-Nam' Hellhounds Race
-Jello People Race
-Simple Ogre Race
-Tyrannosaur Race
-Raptor Race
-Zabrak Race
-Twi'lek Race
-Momu Race
-Ikquan Race
-Warhammer: Fimir

8.6.0
-Kenshi Support typo fix
-Arthropods Race Support update (ShuaPuta)

*Nejarrie
-Race to the Rim Support
-Updated Magical Menagerie Support
-Updated Alpha Animals Support
-Added Support for some VE stuff

8.5.2
-Giant Milk patch
-Incompatible warnings update
-Revia increased support (Herms are rare but are possible, also Nymphs are out for your booty)
-Eldar Milk Fix ?

8.5.1
-changed traits to use spectrum fields (saves will magically lose the existing traits)
-Xenohumans Race Support
-Changed all Pistols to Pistils (this may cause some Genitals to disapear and have to manually be restored)

8.5.0
-Added support for [Grouped Pawns Lists] for the RJW Brothel Tab

8.4.0
-Caisy Rose (ReGrowth Animals support [merged])
-Natsu_Zirok (Translation Russian + Typo fixes [merged])

8.3.3
-fixed typo in Orc female genitals assignment
-Oglis Orks are fungus and have no sex need or RJW parts, they have other ways to reproduce. If you install parts on them they should still be able to get pregnant and impregnate.
-Got rid of moyo patch error (real fix later)

8.3.2
-Moyo milk redone
-Can now turn Moyo milk into DeepBlue serum in drug lab (crafting spot for low tech people) requires research to make DeepBlue serum
-Nyaron BasePawn fix
-Oglis 40k Orks support maybe (needs changes maybe)?

8.3.1
-Rename some files
-Orassan Milk Patch
-CuteBold Milk Patch

8.3.0
-Part props and Typo Fixes(ShuaPuta).
-Cleaning
-Moyo Milk Support

8.2.2
-Kemomimihouse patch updates
-marked real faction guest incompatible as it can disterbe pregnancy
-Fixed Ferian and Lapelli issues maybe
-if milkable colonists is loaded patch races with milk

8.2.1
-updated rjw-ex plug patch to match latest rjw-ex
-waa skeleton race support ?

8.2.0
-Moyo Mechanoids patch
-Disabled the Breeder charm Race patch
-Updated to match latest HAR code for RJW-Ex wear Apperal patch
-Moved all Orassan RJW Support to the Orassans mod

8.1.2
Bovine Penis multi surgery bow require bovine penis and the label is fixed

8.1.1
-New hediffs Descriptions by ShauaPuta
-Rimcraft Races by ShauaPuta
-A few fixes

8.1.0
-Rakkle big tiddy snake girl race
-More horse people added to the Equims race support
-Roaly Thrumbo race support
-big donged horses ?

8.0.1
-Hybrids (Humans, Orassan, Nyaron, Nukos) Fixed ?
-Added Moyo Race Support
-Added Vulpes Race Support
-Fixed Ewok typo
-improved a few patches

8.0.0
-Test Hybrids (Humans, Orassan, Nyaron, Nukos)
-Nyaron Animation Tail Fix

7.8.1
-Reduced rat gestation to 7 days.

7.8.0
-Folder structure
-Version support
-potential fix for surgery
-If Animatioons is loaded patches.

7.7.4
-Fixed Engi Race (LTF)
-Fixed Aerofleet (AA)
-Added Flat Preset
-Refined Protogen Race Support

7.7.3
-fixed the space elves again
-LTF race support (by ShuaPuta)

7.7.2
-support Ancient Species the race mod
-disabled a number of patches dealing with Races Spawn values and Life stages and litter sizes (these started off as patches taken from BnC when the mod was having many issues working with many lists in 1.0) in favor of the patches in several Children or Pregnancy mods.

7.7.1
-fixed large breastss Display issue
-Avali Dino penis is now gone

7.7.0
-added changelog
-tweak breeders delight
-added length and width to custom parts
-mist men now get parts and gender
-Kijin hot Spring grants 10% fertility



